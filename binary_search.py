attempts = 1
index = -1


arr = input("Enter comma-seperated numbers (no space): ")
print("Entered List: ", arr)
n = int(input("Enter a number to be search: "))
list = arr.split(",")

for i in range(0, len(list)):
    list[i] = int(list[i])


def is_sorted():
    flag = 0

    test_list = list[:]
    test_list.sort()

    if test_list == list:
        flag = 1
        return True
    else:
        return False


def binary_search(list, n):
    l = 0
    u = len(list) - 1

    while l <= u:
        mid = (l + u) // 2

        if list[mid] == n:
            globals()["index"] = mid
            return True
        else:
            if list[mid] < n:
                l = mid + 1
                globals()["attempts"] += 1
            else:
                u = mid - 1
                globals()["attempts"] += 1

    return False


if is_sorted():
    if binary_search(list, n):
        print("Found at Index: ", index)
        print("Attempts:" + str(attempts))
    else:
        print("Not found")
else:
    print("List not sorted")
    